import json
import pymongo
import config

client = pymongo.MongoClient(config.mongo)
db = client.Euro2020


def get_teams():
    teams = []
    for team in db.Teams.find():
        teams.append({
            'name': team['name'],
            'group': team['group'],
            'position': team['position']
        })

    return teams


def lambda_handler(event, context):
    return {
        'body': json.dumps(get_teams())
    }
